package fr.minefield.misc;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;

public class Util {

    public static OS getPlatform() {
        String str = System.getProperty("os.name").toLowerCase();
        if (str.contains("win"))
            return OS.WINDOWS;
        if (str.contains("mac"))
            return OS.MACOS;
        if (str.contains("solaris"))
            return OS.SOLARIS;
        if (str.contains("sunos"))
            return OS.SOLARIS;
        if (str.contains("linux"))
            return OS.LINUX;
        if (str.contains("unix"))
            return OS.LINUX;
        return OS.UNKNOWN;
    }

    public static File getClientDirectory(String basename) {
        String homeDirectory = System.getProperty("user.home", ".");
        File localFile;
        switch (Util.getPlatform()) {
            case LINUX:
            case SOLARIS:
                localFile = new File(homeDirectory, '.' + basename + '/');
                break;
            case WINDOWS:
                String str2 = System.getenv("APPDATA");
                String str3 = str2 != null ? str2 : homeDirectory;

                localFile = new File(str3, '.' + basename + '/');
                break;
            case MACOS:
                localFile = new File(homeDirectory, "Library/Application Support/" + basename);
                break;
            default:
                localFile = new File(homeDirectory, basename + '/');
        }
        if ((!localFile.exists()) && (!localFile.mkdirs())) {
            return null;
        }
        return localFile;
    }

    public static enum OS
    {
        LINUX,
        SOLARIS,
        WINDOWS,
        MACOS,
        UNKNOWN;
    }

    public static void openLink(URI paramURI) {
        if (Desktop.isDesktopSupported()) {
            try {
                Desktop.getDesktop().browse(paramURI);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
