package fr.minefield.misc;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {

    private static Properties properties = null;

    public static String getConf(String key) {
        if (properties == null) {
            properties = new Properties();
            try {
                InputStream is = Config.class.getResourceAsStream("/launcher.properties");
                properties.load(is);
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
                properties = null;
                return null;
            } catch (NullPointerException e) {
                e.printStackTrace();
                properties = null;
                return null;
            }
        }
        return properties.getProperty(key);
    }

}
