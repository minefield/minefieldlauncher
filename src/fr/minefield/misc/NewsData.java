package fr.minefield.misc;

public class NewsData {
    public String imageUrl;
    public String title;
    public String url;

    public NewsData(String imageUrl, String title, String url) {
        super();
        this.imageUrl = imageUrl;
        this.title = title;
        this.url = url;
    }
}
