package fr.minefield.misc;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

public class NewsGet {

    public static NewsData getNews() {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse("http://minefield.fr/java/getLastNews.php");
            doc.getDocumentElement().normalize();
            String imageUrl = doc.getElementsByTagName("img").item(0).getTextContent();
            String newsTitle = doc.getElementsByTagName("title").item(0).getTextContent();
            String newsUrl = doc.getElementsByTagName("url").item(0).getTextContent();
            return new NewsData(imageUrl, newsTitle, newsUrl);
        } catch (Exception e) {
            return null;
		}
	}
}