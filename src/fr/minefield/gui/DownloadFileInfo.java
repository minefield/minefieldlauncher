package fr.minefield.gui;

import java.io.File;

public class DownloadFileInfo {

    public DownloadFileInfo(String name, File destination, boolean eraseIfExist) {
        super();
        this.name = name;
        this.destination = destination;
        this.eraseIfExist = eraseIfExist;
    }

    public File getDestination() {
        return destination;
    }

    public String getName() {
        return name;
    }

    public boolean isEraseIfExist() {
        return eraseIfExist;
    }

    private String name;
    private File destination;
    private boolean eraseIfExist;
}
