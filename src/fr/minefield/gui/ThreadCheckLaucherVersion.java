package fr.minefield.gui;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import fr.minefield.misc.Config;
import fr.minefield.misc.Constants;

public class ThreadCheckLaucherVersion implements Runnable{

    public ThreadCheckLaucherVersion(MainWindow window) {
        this.mainWindow = window;
    }

    public void run() {
        try {
            URL urlAvaibleVersions = new URL(Config.getConf("URL_LAUNCHER_VERSION"));
            InputStreamReader is = new InputStreamReader(urlAvaibleVersions.openStream());
            BufferedReader in = new BufferedReader(is);

            String line = in.readLine();
            int version = Integer.parseInt(line);
            // If newer version available
            if (version > Constants.LAUNCHER_VERSION) {
                mainWindow.notifyOutdated();
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    private MainWindow mainWindow;
}
