package fr.minefield.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import fr.minefield.misc.Config;
import fr.minefield.misc.Constants;
import fr.minefield.misc.Util;

public class ActionRunUpdate implements ActionListener {

    public ActionRunUpdate(MainWindow window) {
        mainWindow = window;
        filesToDownload = new ArrayList<DownloadFileInfo>();
    }

    public void actionPerformed(ActionEvent e) {
        mainWindow.onStartUpdate();

        forceUpdate = mainWindow.forceUpdate();

        resetLauncherProfile = mainWindow.isResetLauncherProfile();

        selectedVersion = mainWindow.getSelectedVersion();
        if (mainWindow.getLastSelectedVersion() == null
                || !mainWindow.getLastSelectedVersion().equals(selectedVersion)) {
            forceUpdate = true;
        }

        clientDirectory = Util.getClientDirectory(Config.getConf("BASE_CLIENT_NAME"));
        if (mainWindow.isUseSameMinecraftDirectory()) {
            launcherDirectory = Util.getClientDirectory("minecraft");
        } else {
            launcherDirectory = Util.getClientDirectory(Config.getConf("BASE_CLIENT_NAME"));
        }

        startDownloadThread();
    }

    private void startDownloadThread() {
        Thread t = new Thread(new Runnable() {
            public void run() {
                boolean needUpdate = checkVersion();

                if (needUpdate || forceUpdate) {
                    setListOfFiletoDownloadDependingOnOS();
                    precalculateDownloadSize();
                    if (!downloadFilesToDownload()) {
                        mainWindow.onFailUpdate("FailedDownloadingMinefieldFiles");
                        return;
                    }
                    if (!settingUpLauncherProfile()) {
                        mainWindow.onFailUpdate("FailedSettingUpLauncherProfile");
                        return;
                    }
                    saveClientVersion();
                }
                runMinecraftLauncher();
            }
        });
        t.start();
    }

    private void precalculateDownloadSize() {
        int totalSize = 1; // Not zero, so no divide by 0 after.
        for (DownloadFileInfo downloadFileInfo : filesToDownload) {
            try {
                URL urlDowndloadFile = new URL(Config.getConf("RESSOURCES_DOWNLOAD_URL") + downloadFileInfo.getName());
                URLConnection httpHeadRequest;
                httpHeadRequest = urlDowndloadFile.openConnection();
                httpHeadRequest.setDefaultUseCaches(false);
                if (httpHeadRequest instanceof HttpURLConnection) {
                    ((HttpURLConnection) httpHeadRequest).setRequestMethod("HEAD");
                }
                totalSize += httpHeadRequest.getContentLength();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        downloadTotalSize = totalSize;
    }

    private boolean settingUpLauncherProfile() {
        File profilesFile = new File(launcherDirectory, "launcher_profiles.json");
        if (!profilesFile.exists())
        {
            //FIXME: profile file not found, profiles cannot be setup...
            return false;
        }

        try {
            // Load profile
            FileInputStream fis = new FileInputStream(profilesFile);
            byte[] data = new byte[(int) profilesFile.length()];
            fis.read(data);
            fis.close();

            String json = new String(data, "UTF-8");
            JSONObject profilesJson = new JSONObject(json);

            JSONObject profiles = (JSONObject) profilesJson.get("profiles");
            if (profiles.has(Config.getConf("CLIENT_NAME"))) {
                if (resetLauncherProfile) {
                    JSONObject profile = new JSONObject();
                    profile.put("name", Config.getConf("CLIENT_NAME"));
                    profile.put("gameDir", clientDirectory.toString());
                    profile.put("lastVersionId", Config.getConf("BASE_CLIENT_NAME"));
                    profile.put("useHopperCrashService", false);
                    profiles.put(Config.getConf("CLIENT_NAME"), profile);
                } else {
                    // FIXME: maybe do some check here, and autofix some variables ?
                }
            } else {
                JSONObject profile = new JSONObject();
                profile.put("name", Config.getConf("CLIENT_NAME"));
                profile.put("gameDir", clientDirectory.toString());
                profile.put("lastVersionId", Config.getConf("BASE_CLIENT_NAME"));
                profile.put("useHopperCrashService", false);
                profiles.put(Config.getConf("CLIENT_NAME"), profile);
            }

            // Save profile to file
            fis.close();
            FileWriter fw = new FileWriter(profilesFile);
            fw.write(profilesJson.toString(4));
            fw.close();

            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassCastException e) {
            // bad profiles file
            e.printStackTrace();
        } catch (JSONException e) {
            // bad profiles file
            e.printStackTrace();
        }
        return false;
    }

    private boolean checkVersion() {
        File versionDirectory = new File(launcherDirectory, "versions");
        if (!versionDirectory.exists()) {
            versionDirectory.mkdir();
        }

        File downloadDestination = new File(versionDirectory, Config.getConf("BASE_CLIENT_NAME"));
        if (!downloadDestination.exists()) {
            downloadDestination.mkdir();
        }

        try {
            URL urlDowndloadFile = new URL(Config.getConf("RESSOURCES_DOWNLOAD_URL") + "version.txt");
            InputStream is = urlDowndloadFile.openStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String serverVersion = br.readLine();
            downloadedVersion = serverVersion;
            is.close();

            File localVersionFile = new File(downloadDestination, "version.txt");
            if (localVersionFile.exists()) {
                FileReader fr = new FileReader(localVersionFile);
                BufferedReader brl = new BufferedReader(fr);
                String localVersion = brl.readLine();
                fr.close();
                if (localVersion == null || localVersion.equals("")) {
                    return true;
                }
                return !serverVersion.equals(localVersion);
            }
            return true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void setListOfFiletoDownloadDependingOnOS() {
        File versionDirectory = new File(launcherDirectory, "versions");
        if (!versionDirectory.exists()) {
            versionDirectory.mkdir();
        }
        File downloadDestination = new File(versionDirectory, Config.getConf("BASE_CLIENT_NAME"));
        if (!downloadDestination.exists()) {
            downloadDestination.mkdir();
        }

        filesToDownload.add(new DownloadFileInfo(
                selectedVersion + ".jar",
                new File(downloadDestination, Config.getConf("BASE_CLIENT_NAME") + ".jar"), 
                true)
        );
        filesToDownload.add(new DownloadFileInfo(
                Config.getConf("BASE_CLIENT_NAME") + ".json", 
                new File(downloadDestination, Config.getConf("BASE_CLIENT_NAME") + ".json"),
                true)
        );
        filesToDownload.add(new DownloadFileInfo(
                Config.getConf("BASE_CLIENT_NAME") + "_launcher_profiles.json",
                new File(launcherDirectory, "launcher_profiles.json"),
                false)
        );
    }

    private boolean downloadFilesToDownload() {
        File versionDirectory = new File(launcherDirectory, "versions");
        if (!versionDirectory.exists()) {
            versionDirectory.mkdir();
        }
        File downloadDestination = new File(versionDirectory, Config.getConf("BASE_CLIENT_NAME"));
        if (!downloadDestination.exists()) {
            downloadDestination.mkdir();
        }

        int totalDownload = 0;

        byte[] buffer = new byte[Constants.BUFFER_SIZE];
        for (DownloadFileInfo downloadFileInfo : filesToDownload) {
            try {
                URL urlDowndloadFile = new URL(Config.getConf("RESSOURCES_DOWNLOAD_URL") + downloadFileInfo.getName());
                InputStream is = urlDowndloadFile.openStream();
                File destinationFile = downloadFileInfo.getDestination();
                if (downloadFileInfo.isEraseIfExist() && destinationFile.exists()) {
                    destinationFile.delete();
                }
                if (!downloadFileInfo.isEraseIfExist() && destinationFile.exists()) {
                    if (!mainWindow.forceUpdate()) {
                        continue;
                    }
                    if (mainWindow.isUseSameMinecraftDirectory()) {
                        continue;
                    }
                }
                FileOutputStream destination = new FileOutputStream(destinationFile);
                int readed = is.read(buffer, 0, Constants.BUFFER_SIZE);
                while (readed >= 0) {
                    destination.write(buffer, 0, readed);
                    totalDownload += readed;
                    mainWindow.updateProgress(totalDownload * 100 / downloadTotalSize);
                    readed = is.read(buffer, 0, Constants.BUFFER_SIZE);
                }
                destination.close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    private void saveClientVersion() {
        File versionDirectory = new File(launcherDirectory, "versions");
        if (!versionDirectory.exists()) {
            versionDirectory.mkdir();
        }
        File downloadDestination = new File(versionDirectory, Config.getConf("BASE_CLIENT_NAME"));
        if (!downloadDestination.exists()) {
            downloadDestination.mkdir();
        }

        try {
            if (downloadedVersion != null) {
                File localVersionFile = new File(downloadDestination, "version.txt");
                if (!localVersionFile.exists()) {
                    localVersionFile.createNewFile();
                }
                FileWriter fw = new FileWriter(localVersionFile);
                BufferedWriter brl = new BufferedWriter(fw);
                brl.write(downloadedVersion);
                brl.close();
                fw.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void runMinecraftLauncher() {
        Runtime runtine = Runtime.getRuntime();
        try {
            File pathToMinecraftLauncher = mainWindow.getLauncherPath();
            if (pathToMinecraftLauncher != null) {
                File launcher = pathToMinecraftLauncher;
                if (pathToMinecraftLauncher.getName().endsWith(".app")) {
                    launcher = new File(pathToMinecraftLauncher, "Contents" + File.separator + "Resources"
                            + File.separator + "Java" + File.separator + "Bootstrap.jar");
                }

                if (launcher.canExecute() && !launcher.toString().endsWith(".jar")) {
                    runtine.exec(new String[] {
                        launcher.toString(), "--workDir", launcherDirectory.toString()
                    });
                } else {
                    runtine.exec(new String[] {
                        "java", "-jar", launcher.toString(), "--workDir", launcherDirectory.toString()
                    });
                }
                System.exit(0);
            } else {
                mainWindow.onFailUpdate("ErrorMcLauncherNotFound");
            }
        } catch (IOException e) {
            e.printStackTrace();
            mainWindow.onFailUpdate("ErrorLaunchingMcLauncher");
        }
    }

    private MainWindow mainWindow;
    private boolean forceUpdate;
    private boolean resetLauncherProfile;
    private String selectedVersion;
    private ArrayList<DownloadFileInfo> filesToDownload;
    private File clientDirectory;
    private File launcherDirectory;
    private int downloadTotalSize;
    private String downloadedVersion;
}
