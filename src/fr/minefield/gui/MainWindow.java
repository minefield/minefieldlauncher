package fr.minefield.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;

import fr.minefield.gui.widgets.AntialiaserJlabel;
import fr.minefield.gui.widgets.CustomComboBox;
import fr.minefield.gui.widgets.ImageBlock;
import fr.minefield.gui.widgets.ImageButton;
import fr.minefield.gui.widgets.LoadingBar;
import fr.minefield.gui.widgets.NewsPanel;
import fr.minefield.gui.widgets.SeparatorLine;
import fr.minefield.misc.Config;
import fr.minefield.misc.Constants;
import fr.minefield.misc.MinecraftLauncherFinder;
import fr.minefield.misc.Util;

public class MainWindow extends JFrame {
	protected JPanel advancedPanel;

    public MainWindow() {
        super(Config.getConf("CLIENT_NAME"));
        useSameMinecraftDirectory = true;
    }

    /**
     * Open window
     */
    public void init() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(new Dimension(854, 480)));
        setBackground(Color.WHITE);
        try {
            URL iconInputStream = getClass().getResource("/Minefield-icon-small.png");
            if (iconInputStream != null)
            {
                Image icon = ImageIO.read(iconInputStream);
                if (icon != null)
                {
                    this.setIconImage(icon);
                }
            }
        } catch (IOException localIOException) {
            localIOException.printStackTrace();
        }

        setupLang();
        createWidgets();
        retriveAvaibleVersion();
        MinecraftLauncherFinder.StartSearch(this);


        pack();
        // Center window on the screen
        setLocationRelativeTo(null);
        setVisible(true);
    }
    
    public void redraw() {
    	this.repaint();
    }

    /**
     * Setup the buttons, and various widget. Design of application is here.
     */
    private void createWidgets() {
    	Color sidesBackground = Color.WHITE;
    	Color centerBackground = new Color(230, 231, 225);
    	Color matteBackground = new Color(50, 50, 50);
    	Color progressBar = new Color(180, 60, 30);
    	Color blackText = new Color(39, 39, 39);
    	Color comboText = Color.DARK_GRAY;
    	Color comboBackground = Color.WHITE;
    	
    	UIManager.put("ComboBox.background", new ColorUIResource(UIManager.getColor("TextField.background")));
    	UIManager.put("ComboBox.foreground", new ColorUIResource(UIManager.getColor("TextField.foreground")));
    	UIManager.put("ComboBox.selectionBackground", new ColorUIResource(comboBackground));
    	UIManager.put("ComboBox.selectionForeground", new ColorUIResource(comboText));
    	
    	UIManager.put("ProgressBar.background", matteBackground);
    	UIManager.put("ProgressBar.foreground", progressBar);
    	UIManager.put("ProgressBar.selectionBackground", matteBackground);
    	UIManager.put("ProgressBar.selectionForeground", progressBar);
    	
    	this.setLayout(new BorderLayout());
    	
        JPanel leftWidgetsPanel = new JPanel();
        JPanel middleWidgetsPanel = new JPanel();
        JPanel rightWidgetsPanel = new JPanel();

        leftWidgetsPanel.setLayout(new GridBagLayout());
        middleWidgetsPanel.setLayout(new GridBagLayout());
        rightWidgetsPanel.setLayout(new BorderLayout());
        
        // Left panel
        {
            ImageBlock logo = new ImageBlock(getClass().getResource("/Minefield-icon.png"));
            logo.setPreferredSize(new Dimension(102, 120));
            logo.setOpaque(false);
            GridBagConstraints gc = new GridBagConstraints();
            gc.gridy = 0;
            gc.insets = new Insets(73, 0, 0, 0);
            gc.anchor = GridBagConstraints.NORTH; 
            leftWidgetsPanel.add(logo, gc);

            newVersionAvailable = new AntialiaserJlabel("<html><font color=red>" 
                    + "<center>" + messages.getString("NewVersionAvailable") + "<br>" 
                    + Config.getConf("LAUNCHER_GET_URL") +  "</font></center></html>");
            newVersionAvailable.setForeground(blackText);
            gc = new GridBagConstraints();
            gc.gridy = 5;
            gc.weighty = 1;
            gc.anchor = GridBagConstraints.PAGE_END;
            JLabel tmpLabel = new AntialiaserJlabel();
            tmpLabel.setForeground(blackText);
            tmpLabel.setOpaque(false);
            leftWidgetsPanel.add(tmpLabel, gc);
            newVersionAvailable.setVisible(false);
            
            gc = new GridBagConstraints();
            gc.gridy = 10;
            gc.anchor = GridBagConstraints.PAGE_END; 
            leftWidgetsPanel.add(newVersionAvailable, gc);
            newVersionAvailable.setVisible(false);

        
            JLabel currentVersion = new AntialiaserJlabel("<html>" 
                    + "<center>Version " + Constants.LAUNCHER_VERSION + "</center></html>");
            gc = new GridBagConstraints();
            gc.gridy = 20;
            gc.insets = new Insets(0, 0, 32, 0);
            leftWidgetsPanel.add(currentVersion, gc);
        }


        loadLauncherData();

        // Center panel
        {
            JPanel connectPanel = new JPanel();
            connectPanel.setOpaque(false);
            connectPanel.setLayout(new GridBagLayout());

            // Connect panel
            {
                oneVersionAvailable = new AntialiaserJlabel();
                oneVersionAvailable.setForeground(blackText);
                oneVersionAvailable.setText(Config.getConf("BASE_CLIENT_NAME"));
                oneVersionAvailable.setVisible(true);
                GridBagConstraints gc = new GridBagConstraints();
                gc.gridx = 0;
                gc.gridy = 0;
                gc.insets = new Insets(58, 0, 0, 0);
                gc.anchor = GridBagConstraints.PAGE_START;
                connectPanel.add(oneVersionAvailable, gc);
                
                chooseVersion = new CustomComboBox();
                chooseVersion.setForeground(blackText);
                chooseVersion.setOpaque(false);
                chooseVersion.addItem(Config.getConf("BASE_CLIENT_NAME"));
                chooseVersion.setPreferredSize(new Dimension(230, 30));
                chooseVersion.setVisible(false);
                gc = new GridBagConstraints();
                gc.gridx = 0;
                gc.gridy = 0;
                gc.insets = new Insets(58, 0, 0, 0);
                connectPanel.add(chooseVersion, gc);

                connectButton = new ImageButton(messages.getString("Connection").toUpperCase());
                connectButton.addActionListener(new ActionRunUpdate(this));
                connectButton.setPreferredSize(new Dimension(230, 60));
                connectButton.setMargin(new Insets(0, 0, 0, 0));
                connectButton.setForeground(Color.WHITE);
                connectButton.setBackground(Color.DARK_GRAY);
                connectButton.setFont(new Font(connectButton.getFont().getName(), Font.BOLD, 25));
                URL connectButtonStream = getClass().getResource("/connect-button.png");
                if (connectButtonStream != null) {
                    connectButton.setBackgroundImage(new ImageIcon(connectButtonStream));
                }
                gc = new GridBagConstraints();
                gc.gridx = 0;
                gc.gridy = 1;
                gc.insets = new Insets(5, 0, 0, 0);
                connectPanel.add(connectButton, gc);
                

                downloadProgress = new LoadingBar(0, 100, "/load-bg.png");
                downloadProgress.setPreferredSize(new Dimension(230, 10));
                gc = new GridBagConstraints();
                gc.gridx = 0;
                gc.gridy = 2;
                gc.weighty = 1.0f;
                gc.anchor = GridBagConstraints.PAGE_START;
                gc.insets = new Insets(5, 0, 0, 0);
                connectPanel.add(downloadProgress, gc);


                errorLabel = new AntialiaserJlabel();
                errorLabel.setForeground(Color.RED);
                errorLabel.setVisible(false);
                gc = new GridBagConstraints();
                gc.gridx = 0;
                gc.gridy = 3;
                gc.insets = new Insets(5, 0, 0, 0);
                gc.anchor = GridBagConstraints.PAGE_START;
                connectPanel.add(errorLabel, gc);

                connectPanel.setAlignmentY(TOP_ALIGNMENT);
                connectPanel.setAlignmentX(LEFT_ALIGNMENT);
            }
            GridBagConstraints gcMiddle = new GridBagConstraints();
            gcMiddle.gridy = 0;
            gcMiddle.weighty = 1;
            gcMiddle.anchor = GridBagConstraints.PAGE_START;
            middleWidgetsPanel.add(connectPanel, gcMiddle);

            JPanel advancedOptionPanel = new JPanel();
            advancedOptionPanel.setOpaque(false);
            advancedOptionPanel.setLayout(new GridBagLayout());
            advancedOptionPanel.addMouseListener(new MouseListener() {
                
                @Override
                public void mouseReleased(MouseEvent arg0) { }
                
                @Override
                public void mousePressed(MouseEvent arg0) { }
                
                @Override
                public void mouseExited(MouseEvent arg0) { }
                
                @Override
                public void mouseEntered(MouseEvent arg0) { }
                
                @Override
                public void mouseClicked(MouseEvent arg0) {
                    MainWindow.this.advancedPanel.setVisible(!MainWindow.this.advancedPanel.isVisible());
                }
            });
            {
                JLabel lblAdvanced = new AntialiaserJlabel(messages.getString("AdvancedOptions"));
                lblAdvanced.setFont(new Font(connectButton.getFont().getName(), Font.BOLD, 14));
                lblAdvanced.setForeground(blackText);
                lblAdvanced.setPreferredSize(new Dimension(200, 20));
                GridBagConstraints gc = new GridBagConstraints();
                gc.gridx = 0;
                advancedOptionPanel.add(lblAdvanced, gc);
                
                ImageBlock arrowIcon = new ImageBlock(getClass().getResource("/arrow_bottom.png"));
                arrowIcon.setPreferredSize(new Dimension(10, 6));
                gc.gridx = 1;
                gc.weightx = 1;
                gc.anchor = GridBagConstraints.EAST;
                gc.insets = new Insets(0, 0, 0, 10);
                advancedOptionPanel.add(arrowIcon, gc);
            }
            gcMiddle = new GridBagConstraints();
            gcMiddle.gridy = 10;
            gcMiddle.anchor = GridBagConstraints.PAGE_END;
            middleWidgetsPanel.add(advancedOptionPanel, gcMiddle);

            
            SeparatorLine line = new SeparatorLine();
            line.setPreferredSize(new Dimension(230, 1));
            gcMiddle = new GridBagConstraints();
            gcMiddle.gridy = 15;
            gcMiddle.anchor = GridBagConstraints.PAGE_START;
            gcMiddle.insets = new Insets(3, 0, 10, 0);
            middleWidgetsPanel.add(line, gcMiddle);

            advancedPanel = new JPanel();
            advancedPanel.setOpaque(false);
            advancedPanel.setLayout(new GridBagLayout());
            // Advanced Options panel
            {
                buttonMcLauncher = new ImageButton(messages.getString("SelectLauncher"));
                buttonMcLauncher.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent arg0) {
                        askMinecraftLauncherPath();
                    }
                });

                URL buttonMcLauncherIconStream = getClass().getResource("/choose-exec.png");
                if (buttonMcLauncherIconStream != null) {
                    buttonMcLauncher.setBackgroundImage(new ImageIcon(buttonMcLauncherIconStream));
                }
                buttonMcLauncher.setPreferredSize(new Dimension(230, 36));
                buttonMcLauncher.setMargin(new Insets(0, 0, 0, 0));
                buttonMcLauncher.setForeground(Color.WHITE);
                buttonMcLauncher.setFont(new Font(connectButton.getFont().getName(), Font.BOLD, 12));                
                GridBagConstraints gc = new GridBagConstraints();
                gc.gridy = 10;
                gc.insets = new Insets(0, 0, 10, 0);
                advancedPanel.add(buttonMcLauncher, gc);

                labelMcLauncher = new AntialiaserJlabel();
                labelMcLauncher.setForeground(blackText);
                updateDisplayMcLauncherPath();
                gc = new GridBagConstraints();
                gc.gridy = 20;
                gc.insets = new Insets(0, 0, 10, 0);
                advancedPanel.add(labelMcLauncher, gc);

                URL checkboxIconPath = getClass().getResource("/coche.png");
                URL checkboxSelectedIconPath = getClass().getResource("/cochee.png");
                ImageIcon checkboxIcon = null;
                if (checkboxIconPath != null) {
                    checkboxIcon = new ImageIcon(checkboxIconPath);
                }
                ImageIcon checkboxSelectedIcon = null;
                if (checkboxIconPath != null) {
                    checkboxSelectedIcon = new ImageIcon(checkboxSelectedIconPath);
                }
                forceUpdate = new JCheckBox();
                forceUpdate.setOpaque(false);
                forceUpdate.setIcon(checkboxIcon);
                forceUpdate.setSelectedIcon(checkboxSelectedIcon);
                forceUpdate.setText(messages.getString("ForceUpdate"));
                gc = new GridBagConstraints();
                gc.gridy = 30;
                gc.anchor = GridBagConstraints.WEST;
                advancedPanel.add(forceUpdate, gc);

                checkUseSameMinecraftDirectory = new JCheckBox();
                checkUseSameMinecraftDirectory.setOpaque(false);
                checkUseSameMinecraftDirectory.setIcon(checkboxIcon);
                checkUseSameMinecraftDirectory.setSelectedIcon(checkboxSelectedIcon);
                checkUseSameMinecraftDirectory.setText(messages.getString("useSameMinecraftDirectory"));
                checkUseSameMinecraftDirectory.setSelected(useSameMinecraftDirectory);
                this.advancedPanel.add(checkUseSameMinecraftDirectory);
                gc = new GridBagConstraints();
                gc.gridy = 40;
                gc.anchor = GridBagConstraints.WEST;
                advancedPanel.add(checkUseSameMinecraftDirectory, gc);

                checkResetLauncherProfile = new JCheckBox();
                checkResetLauncherProfile.setOpaque(false);
                checkResetLauncherProfile.setIcon(checkboxIcon);
                checkResetLauncherProfile.setSelectedIcon(checkboxSelectedIcon);
                checkResetLauncherProfile.setText(messages.getString("resetLauncherProfile"));
                this.advancedPanel.add(checkResetLauncherProfile);
                gc = new GridBagConstraints();
                gc.gridy = 50;
                gc.anchor = GridBagConstraints.WEST;
                advancedPanel.add(checkResetLauncherProfile, gc);
                advancedPanel.setVisible(false);
            }

            gcMiddle = new GridBagConstraints();
            gcMiddle.gridy = 20;
            gcMiddle.insets = new Insets(0, 0, 50, 0);
            gcMiddle.anchor = GridBagConstraints.PAGE_END;
            middleWidgetsPanel.add(advancedPanel, gcMiddle);

        }
        
        checkLauncherVersion();
        
        NewsPanel newsPanel = new NewsPanel(this);
        
        leftWidgetsPanel.setBackground(sidesBackground);
        middleWidgetsPanel.setBackground(centerBackground);
        rightWidgetsPanel.setBackground(sidesBackground);
        
        rightWidgetsPanel.add(newsPanel);

        this.add(leftWidgetsPanel, BorderLayout.WEST);
        this.add(middleWidgetsPanel, BorderLayout.CENTER);
        this.add(rightWidgetsPanel, BorderLayout.EAST);
        
        leftWidgetsPanel.setPreferredSize(new Dimension(256, 30000));
        rightWidgetsPanel.setPreferredSize(new Dimension(256, 30000));

        leftWidgetsPanel.setVisible(true);
        middleWidgetsPanel.setVisible(true);
        rightWidgetsPanel.setVisible(true);
    }

    public void notifyOutdated() {
        newVersionAvailable.setVisible(true);
    }

    /**
     * Called when the update fail, then it unlock interface and should display
     * message
     * 
     * @param message
     *            errorMessageToken (for localised string)
     */
    public void onFailUpdate(String message) {
        connectButton.setEnabled(true);
        chooseVersion.setEnabled(true);
        buttonMcLauncher.setEnabled(true);
        downloadProgress.setValue(0);
        forceUpdate.setEnabled(true);
        checkResetLauncherProfile.setEnabled(true);
        checkUseSameMinecraftDirectory.setEnabled(true);
        errorLabel.setText(messages.getString(message));
        errorLabel.setVisible(true);
    }

    /**
     * Called when the update start, it lock all interface
     */
    public void onStartUpdate() {
        connectButton.setEnabled(false);
        chooseVersion.setEnabled(false);
        buttonMcLauncher.setEnabled(false);
        checkResetLauncherProfile.setEnabled(false);
        errorLabel.setVisible(false);
        forceUpdate.setEnabled(false);
        checkUseSameMinecraftDirectory.setEnabled(false);
        saveLauncherData();
    }

    private void setupLang() {
        currentLocale = new Locale("fr", "FR");
        messages = ResourceBundle.getBundle("StringLangs", currentLocale);
    }
    
    public void addAvailableVersion(String line) {
        oneVersionAvailable.setVisible(false);
        chooseVersion.setVisible(true);
        chooseVersion.addItem(line);
        if (lastSelectedVersion != null && line.equals(lastSelectedVersion)) {
            chooseVersion.setSelectedIndex(chooseVersion.getItemCount() - 1);
        }
    }

    public String getSelectedVersion() {
        return (String) chooseVersion.getSelectedItem();
    }

    /**
     * Save preference of this launcher into a file stored in client directory.
     * 
     * ${HOME}/.XXXXX/prelauncher.properties
     * %APPDATA%/.XXXXX/prelauncher.properties
     */
    private void saveLauncherData() {
        File clientDirectory = Util.getClientDirectory(Config.getConf("BASE_CLIENT_NAME"));
        if (!clientDirectory.exists()) {
            clientDirectory.mkdir();
        }

        try {
            File prelauncherProperties = new File(clientDirectory, "prelauncher.properties");
            Properties properties = new Properties();

            properties.setProperty("lastVersion", (String) chooseVersion.getSelectedItem());

            if (minecraftLauncherPath != null && minecraftLauncherPath.exists()) {
                properties.setProperty("mcLauncher", minecraftLauncherPath.toString());
            }

            useSameMinecraftDirectory = checkUseSameMinecraftDirectory.isSelected();
            properties.setProperty("useSameMinecraftDirectory", useSameMinecraftDirectory ? "true" : "false");

            FileOutputStream fos = new FileOutputStream(prelauncherProperties);
            properties.store(fos, null);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load saved preference of this launcher from file stored in client directory.
     * 
     * ${HOME}/.XXXXX/prelauncher.properties
     * %APPDATA%/.XXXXX/prelauncher.properties
     */
    private void loadLauncherData() {
        File clientDirectory = Util.getClientDirectory(Config.getConf("BASE_CLIENT_NAME"));
        if (clientDirectory.exists()) {
            try {
                File prelauncherProperties = new File(clientDirectory, "prelauncher.properties");
                if (prelauncherProperties.exists()) {
                    FileInputStream fis = new FileInputStream(prelauncherProperties);
                    Properties properties = new Properties();
                    properties.load(fis);

                    lastSelectedVersion = properties.getProperty("lastVersion");

                    minecraftLauncherPath = null;
                    String mcLauncher = properties.getProperty("mcLauncher");
                    if (mcLauncher != null) {
                        minecraftLauncherPath = new File(mcLauncher);
                    }

                    String useMcDir =  properties.getProperty("useSameMinecraftDirectory");
                    useSameMinecraftDirectory = false;
                    if (useMcDir != null) {
                        useSameMinecraftDirectory = useMcDir.equals("true");
                    }

                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Start a thread that will download list of avaible version, and fill the
     * comboBox with each.
     */
    private void retriveAvaibleVersion() {
        Thread thread = new Thread(new ThreadGetAvaibleVersion(this));
        thread.start();
    }

    private void checkLauncherVersion() {
        Thread thread = new Thread(new ThreadCheckLaucherVersion(this));
        thread.start();
    }

    public void updateProgress(int i) {
        downloadProgress.setValue(i);
    }

    public void UpdateMinecraftLauncherPath() {
        if (minecraftLauncherPath == null || !minecraftLauncherPath.exists()) {
            if (!MinecraftLauncherFinder.searching) {
                minecraftLauncherPath = MinecraftLauncherFinder.GetMinecraftLauncherPath();
            }
        }
        updateDisplayMcLauncherPath();
    }

    private void askMinecraftLauncherPath() {
        if (minecraftLauncherPath == null || !minecraftLauncherPath.exists()) {
            if (!MinecraftLauncherFinder.searching) {
                minecraftLauncherPath = MinecraftLauncherFinder.GetMinecraftLauncherPath();
                if (minecraftLauncherPath != null && minecraftLauncherPath.exists()) {
                    updateDisplayMcLauncherPath();
                    return;
                }
            }
        }

        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new MinecraftLauncherFilter());
        int returnVal = fc.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            minecraftLauncherPath = fc.getSelectedFile();
        }
        updateDisplayMcLauncherPath();
    }

    private void updateDisplayMcLauncherPath() {
        
        if (MinecraftLauncherFinder.searching) {
            labelMcLauncher.setText("<html><center>" 
                    + messages.getString("McLauncherLabel") 
                    + "<br>"
                    + messages.getString("Searching")
                    + "</center></html>");
        } else if (minecraftLauncherPath == null || !minecraftLauncherPath.exists()) {
            labelMcLauncher.setText("<html><center>" 
                    + messages.getString("McLauncherLabel") 
                    + "<br>"
                    + messages.getString("NotFound")
                    + "</center></html>");
        } else {
            labelMcLauncher.setText("<html><center>" 
                    + messages.getString("McLauncherLabel") 
                    + "<br>"
                    + minecraftLauncherPath
                    + "</center></html>");
            saveLauncherData();
        }
    }

    public File getLauncherPath() {
        if (minecraftLauncherPath == null) {
            askMinecraftLauncherPath();
            if (minecraftLauncherPath == null) {
                return null;
            }
        }
        if (!minecraftLauncherPath.exists()) {
            askMinecraftLauncherPath();
        } else {
            return minecraftLauncherPath;
        }
        return null;
    }

    public boolean forceUpdate() {
        return forceUpdate.isSelected();
    }

    public boolean isUseSameMinecraftDirectory() {
        return useSameMinecraftDirectory;
    }

    public boolean isResetLauncherProfile() {
        return checkResetLauncherProfile.isSelected();
    }

    public String getLastSelectedVersion() {
        return lastSelectedVersion;
    }

    private static final long serialVersionUID = -3964501094848191700L;
    private File minecraftLauncherPath;
    private JComboBox chooseVersion;
    private JLabel oneVersionAvailable;
    private ImageButton connectButton;
    private LoadingBar downloadProgress;
    private JLabel errorLabel;
    private String lastSelectedVersion;
    private Locale currentLocale;
    private JLabel newVersionAvailable;
    private ResourceBundle messages;
    private JCheckBox forceUpdate;
    private ImageButton buttonMcLauncher;
    private JLabel labelMcLauncher;
    private boolean useSameMinecraftDirectory;
    private JCheckBox checkUseSameMinecraftDirectory;
    private JCheckBox checkResetLauncherProfile;
}
