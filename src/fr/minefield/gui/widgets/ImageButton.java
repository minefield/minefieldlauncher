package fr.minefield.gui.widgets;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.GroupLayout.Alignment;
import javax.swing.Icon;
import javax.swing.JButton;

import fr.minefield.gui.widgets.ui.ImageButtonUI;

public class ImageButton extends JButton {

    private static final long serialVersionUID = 1L;
    protected Icon background;
    protected Color customBackgroundColor;
    protected Insets transparentPadding;
    protected Insets immageOffset;
    protected Alignment xAlignement;
    protected Alignment yAlignement;

    public ImageButton(String text) {
        super(text);
        setContentAreaFilled(false);
        setBorderPainted(false);
        setOpaque(false);
        setBorder(null);
        setUI(new ImageButtonUI());
        xAlignement = Alignment.BASELINE;
        yAlignement = Alignment.BASELINE;
        customBackgroundColor = null;
    }

    @Override
    public void paint(Graphics g) {
        if (customBackgroundColor != null) {
            g.setColor(customBackgroundColor);
            Dimension size = getSize();
            if (this.transparentPadding != null) {
                g.fillRect(transparentPadding.left, transparentPadding.top, (int) size.getWidth()
                        - transparentPadding.right, (int) size.getHeight() - transparentPadding.bottom);
            } else {
                g.fillRect(0, 0, (int) size.getWidth(), (int) size.getHeight());
            }
        }
        if (!getModel().isEnabled()) {
            setBackground(Color.DARK_GRAY);
        } else if (getModel().isRollover()) {
            setForeground(Color.LIGHT_GRAY);
        } else {
            setForeground(Color.WHITE);
        }

        if (background != null) {
            int x = 0;
            int y = 0;
            Dimension size = getSize();
            if (xAlignement == Alignment.CENTER) {
                x = (size.width - background.getIconWidth()) / 2;
            }
            if (yAlignement == Alignment.CENTER) {
                y = (size.height - background.getIconWidth()) / 2;
            }
            if (immageOffset != null) {
                x += immageOffset.left;
                y += immageOffset.top;
            }
            background.paintIcon(this, g, x, y);
        }

        super.paint(g);
    }

    public void setImmageOffset(Insets immageOffset) {
        this.immageOffset = immageOffset;
    }

    public void setCustomBackgroundColor(Color customBackgroundColor) {
        this.customBackgroundColor = customBackgroundColor;
    }

    public void setBackgroundImage(Icon imageIcon) {
        this.background = imageIcon;
    }

    public void setTransparentPadding(Insets insets) {
        this.transparentPadding = insets;
    }

    public void setxAlignement(Alignment xAlignement) {
        this.xAlignement = xAlignement;
    }

    public void setyAlignement(Alignment yAlignement) {
        this.yAlignement = yAlignement;
    }
}
