package fr.minefield.gui.widgets.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;

import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.plaf.basic.BasicComboBoxUI;

import fr.minefield.gui.widgets.CustomComboBoxButton;
import fr.minefield.gui.widgets.ImageButton;

public class CustonComboBoxUI extends BasicComboBoxUI {

    @Override
    public void paintCurrentValue(Graphics g, Rectangle bounds, boolean hasFocus) {
        super.paintCurrentValue(g, bounds, hasFocus);
    }

    @Override
    protected JButton createArrowButton() {
        ImageButton button = new CustomComboBoxButton("");
        button.setCustomBackgroundColor(Color.WHITE);
        button.setxAlignement(Alignment.CENTER);
        button.setyAlignement(Alignment.CENTER);
        button.setTransparentPadding(new Insets(0, 1, 0, 25));
        ImageIcon icon = new ImageIcon(getClass().getResource("/arrow_bottom.png"));
        button.setBackgroundImage(icon);
        button.setImmageOffset(new Insets(4, 0, 0, 0));
        return button;
    }
}
