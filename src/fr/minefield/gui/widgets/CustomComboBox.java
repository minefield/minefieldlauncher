package fr.minefield.gui.widgets;

import java.awt.Graphics;

import javax.swing.JComboBox;

import fr.minefield.gui.widgets.ui.CustonComboBoxUI;
import fr.minefield.gui.widgets.ui.RoundedCornerBorder;

public class CustomComboBox extends JComboBox {

    // paintCurrentValueBackground
    private static final long serialVersionUID = 1L;

    public CustomComboBox() {
        super();
        setBorder(new RoundedCornerBorder());
        setUI(new CustonComboBoxUI());
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
    }
}
