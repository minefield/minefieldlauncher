package fr.minefield.gui.widgets;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class ImageBlock extends JPanel {

    private static final long serialVersionUID = -3288970260993320025L;

    protected BufferedImage img;

    public ImageBlock() {
    }

    public ImageBlock(URL path) {
        LoadImage(path);
    }

    public void LoadImage(final URL path) {
        
        Thread t = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    img = ImageIO.read(path);
                    invalidate();
                    repaint();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (img != null) {
            Graphics2D g2d = (Graphics2D)g;
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            int width = (int) getPreferredSize().getWidth();
            int height = (int) getPreferredSize().getHeight();
            g.drawImage(this.img, 0, 0, width, height, null);
            g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_DEFAULT);
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        }
    }
}
