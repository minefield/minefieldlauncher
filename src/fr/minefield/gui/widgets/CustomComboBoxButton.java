package fr.minefield.gui.widgets;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

public class CustomComboBoxButton extends ImageButton {

    private static final long serialVersionUID = 1L;

    public CustomComboBoxButton(String text) {
        super(text);
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(customBackgroundColor);
        if (customBackgroundColor != null) {
            Dimension size = getSize();
            g.fillRoundRect(1, 0, (int) size.getWidth(), (int) size.getHeight(), 8, 12);
        }
        super.paint(g);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
    }
}
