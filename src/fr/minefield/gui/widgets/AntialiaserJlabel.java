package fr.minefield.gui.widgets;

import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.Icon;
import javax.swing.JLabel;

public class AntialiaserJlabel extends JLabel {

    private static final long serialVersionUID = 1L;

    public AntialiaserJlabel() {
        super();
    }

    public AntialiaserJlabel(Icon image, int horizontalAlignment) {
        super(image, horizontalAlignment);
    }

    public AntialiaserJlabel(Icon image) {
        super(image);
    }

    public AntialiaserJlabel(String text, Icon icon, int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
    }

    public AntialiaserJlabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
    }

    public AntialiaserJlabel(String text) {
        super(text);
    }

    @Override
    public void paint(java.awt.Graphics g) {
        Graphics2D g2d = (Graphics2D)g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        super.paint(g);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
    };

}
