package fr.minefield.gui.widgets;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

public class SeparatorLine extends JPanel {
    private static final long serialVersionUID = 1L;

    @Override
    public void paint(Graphics g) {
        Dimension d = this.getSize();

        g.setColor(new Color(165, 165, 165));
        g.drawLine(0, d.height / 2, d.width, d.height / 2);
    }
}
