package fr.minefield.gui.widgets;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JProgressBar;

public class LoadingBar extends JProgressBar {

    private static final long serialVersionUID = 1L;
    private Image bgImage;
    private Color progressColor;

    public LoadingBar(int min, int max, String imagePath) {
        super(min, max);
        progressColor = new Color(175, 33, 32);
        URL imageBackgroundPath = getClass().getResource(imagePath);
        if (imagePath != null)
        {
            ImageIcon img = new ImageIcon(imageBackgroundPath);
            bgImage = img.getImage();
        }
    }
    
    @Override
    public void paint(Graphics g) {
        if (bgImage != null)
        {
            g.drawImage(bgImage, 0, 0, null);
            g.setColor(progressColor);
            g.fillRect(3, 3, (int) (getPercentComplete() * getWidth()), 4);
        }
    }
}
