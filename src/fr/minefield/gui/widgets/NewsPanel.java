package fr.minefield.gui.widgets;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.minefield.gui.MainWindow;
import fr.minefield.misc.NewsData;
import fr.minefield.misc.NewsGet;
import fr.minefield.misc.Util;

public class NewsPanel extends JPanel {

    private static final long serialVersionUID = 8759682315921123984L;
    private Locale currentLocale;
    private ResourceBundle messages;
    private ImageBlock newsImage;
    private JLabel newsTitle;
    private URI newsLink;

    class GotoNewsListener implements MouseListener
    {
        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (newsLink != null) {
                Util.openLink(newsLink);
            }
        }
    }
    
    public NewsPanel(MainWindow mainWindow) {
        setOpaque(false);

        newsLink = null;
        this.currentLocale = new Locale("fr", "FR");
        this.messages = ResourceBundle.getBundle("StringLangs", currentLocale);

        setLayout(new GridBagLayout());
        JPanel content = new JPanel();
        content.setOpaque(false);
        content.setLayout(new GridBagLayout());
        {
            JLabel newsLabel = new AntialiaserJlabel(messages.getString("newsHtmlTitle"));
            newsLabel.setFont(new Font(newsLabel.getFont().getName(), Font.BOLD, 25));
            newsLabel.setPreferredSize(new Dimension(210, 20));
            newsLabel.setOpaque(false);
            GridBagConstraints gc = new GridBagConstraints();
            gc.gridy = 0;
            gc.weighty = 1;
            gc.anchor = GridBagConstraints.SOUTHWEST;
            content.add(newsLabel, gc);

            SeparatorLine line = new SeparatorLine();
            line.setPreferredSize(new Dimension(210, 1));
            gc = new GridBagConstraints();
            gc.gridy = 1;
            gc.insets = new Insets(5, 0, 5, 0);
            content.add(line, gc);

            newsTitle = new AntialiaserJlabel();
            newsTitle.setPreferredSize(new Dimension(210, 15));
            newsTitle.setOpaque(false);
            newsTitle.setText("blabalwd dasdas dsad sa sda dasopd asposd");
            newsTitle.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            newsTitle.addMouseListener(new GotoNewsListener());
            gc = new GridBagConstraints();
            gc.gridy = 2;
            gc.fill = GridBagConstraints.NONE;
            content.add(newsTitle, gc);

            // FIXME: place holder
            newsImage = new ImageBlock();
            newsImage.setPreferredSize(new Dimension(210, 210));
            newsImage.setOpaque(false);
            newsImage.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            newsImage.addMouseListener(new GotoNewsListener());
            gc = new GridBagConstraints();
            gc.gridy = 3;
            gc.insets = new Insets(5, 0, 5, 0);
            content.add(newsImage, gc);

            JLabel readMoreLabel = new AntialiaserJlabel(messages.getString("newsHtmlLinkLabel"));
            readMoreLabel.setOpaque(false);
            readMoreLabel.setEnabled(true);
            readMoreLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            readMoreLabel.addMouseListener(new GotoNewsListener());

            gc = new GridBagConstraints();
            gc.gridy = 4;
            gc.weighty = 1;
            gc.anchor = GridBagConstraints.WEST;
            content.add(readMoreLabel, gc);
        }
        add(content);
        startGetNews();

    }

    public void setNewsData(NewsData newsData) {
        if (newsData != null) {
            try {
                newsImage.LoadImage(new URL(newsData.imageUrl));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            newsTitle.setText(newsData.title);
            try {
                newsLink = new URI(newsData.url);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    public void startGetNews() {
        Thread t = new Thread(new Runnable() {

            @Override
            public void run() {
                NewsData data = NewsGet.getNews();
                setNewsData(data);
            }
        });
        t.start();
    }
}
