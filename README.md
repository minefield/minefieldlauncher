# Minefield launcher
This project is the minefield.fr launcher that can be found at http://minefield.fr/.
You are not allowed to use this source code.
If you want create your own launcher, please see this project which is free:
https://bitbucket.org/minefield/modpacklauncher

## Prerequisite

 * gmake (`make` on linux)
 * java compiler, version 1.6 (type `javac -version` to check it)
 * `launch4j` for windows binary generation only: `http://launch4j.sourceforge.net/`
 * `ant` with `AppBundler` for mac application generation `https://github.com/joshmarinacci/AppBundler`
 * `genisoimage` for mac installer

## Compiling

### Linux

First you have to setup some ressources files, and define the name of your launcher.
Start by executing the script `install.sh` and answer to the questions.

    ./install.sh
    
This will create the following files:
 * `Launcher.cfg` used by the Makefile.
 * `build.properties` used by ant (`build.xml`) to generate mac application (`.app`)
 * `ressources/${LAUNCER_NAME}.properties` the configuration files of your launcher. 
 * `bundle-${LAUNCER_NAME}.xml` only if you need to build mac installer (`.dmg`)

Now you have to fill the variables in `ressources/${LAUNCER_NAME}.properties`

 * `CLIENT_NAME` is the display name of you launcher
 * `BASE_CLIENT_NAME` internal name of you launcher
 * `LAUNCHER_GET_URL` your website where the launcher can be download (used in message to notify user that there is a new version of the launcher)
 * `RESSOURCES_DOWNLOAD_URL` url to directory where you store your modpack, and other stuff to be downloaded
 * `URL_AVAIBLE_VERSIONS` url to list of alternative modpack (optionnal)
 * `URL_LAUNCHER_VERSION` url to file containing version of the latest launcher

Now you can build the .jar

    make

if you have a warning about 1.6: `warning: [options] bootstrap class path not set in conjunction with -source 1.6`
you are not the good java compiler version, you can change it by defining environement variable JAVAC

    JAVAC=/path/to/jdk/1.6/bin/javac make

To make windows binary, use

    make windows
    
If launch4j is not found, define path with
    
    LAUNCH4j=/path/to/launch4j make windows
    
To build the Mac application:

    make mac
  
### Others

    Not supported yet, but you are free to do it and share this.


## Mac installer

For generating mac installer

    make mac_installer
    
If you want a beautifull installer, you need a mac. Setup the position of icon, 
the background etc.. then copy the .DS_Store and you background image in `ressources`
folder with the folowing name

 * ${LAUNCER_NAME}_background.png
 * ${LAUNCER_NAME}.DS_Store
 
then 

    make mac_installer

Have fun
