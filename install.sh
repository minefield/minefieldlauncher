#! /bin/sh

echo "Enter name of you launcher:"
echo -n "> "
read name
echo LAUNCHER_NAME=${name}> Launcher.cfg
echo LauncherName=${name}> build.properties
if test -z "${name}"
then
  exit 1
fi

if test ! -e ressources/${name}.properties
then
  cp ressources/launcher.properties.default ressources/${name}.properties
  echo "Fill the file ressources/${name}.properties with correct value, then run make command"
fi
echo "Do you want build mac installer (*.dmg) ? (y/N)"
read yesno
if test "${yesno}" = 'y'
then
  echo "You have to create and add the following file into the ressources/ directory:"
  echo "-${name}_background.png"
  echo "-${name}.DS_Store"
  echo "-${name}.icns"
  
  echo "
  <app name=\"${name}\">
    <jar name=\"${name}.jar\" main-class=\"Main\"/>
  </app>" > bundle-${name}.xml

  echo "Also you have to install https://github.com/joshmarinacci/AppBundler"
fi
