JAVAC?=javac
JAVAVERSION=1.6
JAVACFLAGS=-source $(JAVAVERSION) -target $(JAVAVERSION)
LAUNCH4J?=launch4j

include Launcher.cfg

SOURCES_FILES=Main.java                                                  \
              fr/minefield/misc/MinecraftLauncherFinder.java             \
              fr/minefield/misc/Util.java                                \
              fr/minefield/misc/NewsData.java                            \
              fr/minefield/misc/NewsGet.java                             \
              fr/minefield/misc/Config.java                              \
              fr/minefield/misc/Constants.java                           \
              fr/minefield/gui/MinecraftLauncherFilter.java              \
              fr/minefield/gui/ActionRunUpdate.java                      \
              fr/minefield/gui/ThreadCheckLaucherVersion.java            \
              fr/minefield/gui/MainWindow.java                           \
              fr/minefield/gui/widgets/CustomComboBox.java               \
              fr/minefield/gui/widgets/CustomComboBoxButton.java         \
              fr/minefield/gui/widgets/ImageButton.java                  \
              fr/minefield/gui/widgets/LoadingBar.java                   \
              fr/minefield/gui/widgets/ui/CustonComboBoxUI.java          \
              fr/minefield/gui/widgets/ui/ImageButtonUI.java             \
              fr/minefield/gui/widgets/ui/RoundedCornerBorder.java       \
              fr/minefield/gui/widgets/SeparatorLine.java                \
              fr/minefield/gui/widgets/ImageBlock.java                   \
              fr/minefield/gui/widgets/AntialiaserJlabel.java            \
              fr/minefield/gui/widgets/NewsPanel.java                    \
              fr/minefield/gui/DownloadFileInfo.java                     \
              fr/minefield/gui/ThreadGetAvaibleVersion.java              \
              org/json/JSONString.java                                   \
              org/json/XML.java                                          \
              org/json/Kim.java                                          \
              org/json/JSONWriter.java                                   \
              org/json/Property.java                                     \
              org/json/JSONArray.java                                    \
              org/json/Cookie.java                                       \
              org/json/JSONObject.java                                   \
              org/json/CDL.java                                          \
              org/json/JSONException.java                                \
              org/json/JSONML.java                                       \
              org/json/JSONStringer.java                                 \
              org/json/HTTPTokener.java                                  \
              org/json/HTTP.java                                         \
              org/json/CookieList.java                                   \
              org/json/JSONTokener.java                                  \
              org/json/XMLTokener.java



SRC_DIR=src/
RES_DIR=ressources/
BUILD_DIR=build/
TMP_DIR=tmp

RESSOURCES= StringLangs_fr_FR.properties            \
			StringLangs.properties      \
			arrow_bottom.png            \
			choose-exec.png             \
			cochee.png                  \
			coche.png                   \
			connect-button.png          \
			dropdown-bg.png             \
			load-bg.png                 \
			Minefield-icon.png			\
			Minefield-icon-small.png

LAUNCHER_PROPERTIES = $(RES_DIR)/$(LAUNCHER_NAME).properties

MAC_BACKGROUND_IMAGE=$(RES_DIR)/$(LAUNCHER_NAME)_background.png
MAC_DS_STORE=$(RES_DIR)/$(LAUNCHER_NAME).DS_Store

CLASS_FILES=$(SOURCES_FILES:.java=.class)

SRC=$(addprefix $(SRC_DIR), $(SOURCES_FILES))
OBJ=$(addprefix $(BUILD_DIR), $(CLASS_FILES))
RES=$(addprefix $(RES_DIR), $(RESSOURCES))
TARGET=$(LAUNCHER_NAME).jar
MAC_APP_DIR=dist/mac/$(LAUNCHER_NAME).app
TARGET_MAC=$(LAUNCHER_NAME).dmg
ICON_PNG=$(RES_DIR)/$(LAUNCHER_NAME)-icon.png
ICON_ICO=$(RES_DIR)/$(LAUNCHER_NAME)-icon.ico
ICON_ICNS=$(RES_DIR)/$(LAUNCHER_NAME)-icon.icns


all: $(TARGET)
	@echo Done

windows:
	$(LAUNCH4J) $(RES_DIR)/$(LAUNCHER_NAME)Launch4jConfig.xml

mac: $(MAC_APP_DIR)

mac_installer: $(MAC_APP_DIR) $(TARGET_MAC)

$(MAC_APP_DIR): $(TARGET)

$(TMP_DIR):
	mkdir $@

$(MAC_APP_DIR):
	ant mac_application
	-cp $(ICON_ICNS) $(MAC_APP_DIR)/Contents/Resources/icon.icns

$(MAC_BACKGROUND_IMAGE):
	@echo [WARNING]Missing $(MAC_BACKGROUND_IMAGE), this will not be add to installer

$(MAC_DS_STORE):
	@echo [WARNING]Missing $(MAC_DS_STORE), this will not be add to installer


$(TARGET_MAC): $(MAC_APP_DIR) $(MAC_DS_STORE) $(MAC_BACKGROUND_IMAGE) $(TMP_DIR)
	cp -r $(MAC_APP_DIR) $(TMP_DIR)
	ln -s /Applications $(TMP_DIR)/Applications
	mkdir $(TMP_DIR)/.background
	-cp $(MAC_BACKGROUND_IMAGE) $(TMP_DIR)/.background/background.png
	-cp $(MAC_DS_STORE) $(TMP_DIR)/.DS_Store
	genisoimage -V $(LAUNCHER_NAME) -D -R -apple -no-pad -o $(TARGET_MAC) $(TMP_DIR)
	$(RM) -r $(TMP_DIR)

launcher.properties:
	@echo You must setup launcher.properties before building.

$(BUILD_DIR): 
	mkdir -p $(BUILD_DIR)

$(TARGET): $(BUILD_DIR) $(RES) $(SRC)
	$(JAVAC) $(JAVACFLAGS) -d $(BUILD_DIR) $(SRC)
	cp $(RES) $(BUILD_DIR)
	cp $(LAUNCHER_PROPERTIES) $(BUILD_DIR)/launcher.properties
	jar cfe $(TARGET) Main -C $(BUILD_DIR) .

clean:
	$(RM) $(TARGET) $(OBJ) $(TARGET_MAC)
	$(RM) -r $(MAC_APP_DIR) $(TMP_DIR) $(BUILD_DIR) dist

distclean: clean
	$(RM) -r lib
	$(RM) Launcher.cfg build.properties bundle-$(LAUNCHER_NAME).xml
	
.PHONY=clean all mac
